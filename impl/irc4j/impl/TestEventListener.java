package irc4j.impl;

import irc4j.EventListener;
import irc4j.event.ConnectEvent;
import irc4j.event.MessageEvent;

import java.util.Arrays;
import java.util.List;

public class TestEventListener extends EventListener {
    @Override
    public void onMessageEvent(MessageEvent event) {
        System.out.println("Message: " + event.getUser() + " Channel: " + event.getChannel() + " Message: " + event.getMessage());
        List<String> message = Arrays.asList(event.getMessage().split(" "));
        if (event.getMessage().startsWith("!")) {
            if (message.get(0).equals("!example")) {
                event.getBot().sendRawLine("PRIVMSG " + event.getChannel() + " :" + event.getUser().substring(1, event.getUser().indexOf('!')) + ": Hi!");
            } else if (message.get(0).equals("!quit")) {
                event.getBot().disconnect();
            }
        }
    }

    @Override
    public void onConnectEvent(ConnectEvent event) {
        event.getBot().sendRawLine("JOIN #kaendfinger");
    }
}
