package irc4j.impl;

import irc4j.IRC4J;
import irc4j.IRC4JConfig;

import java.io.IOException;
import java.io.PrintWriter;

public class TestBot {

    public static void main(String[] args) {
        IRC4JConfig config = new IRC4JConfig();
        config.setServer("irc.esper.net", 6667);
        config.setNickname("IRC4J");
        config.setRealName("IRC4J");
        config.setUsername("IRC4J");
        IRC4J bot = config.create();
        bot.setEventListener(new TestEventListener());
        try {
            bot.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
