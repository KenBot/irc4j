package irc4j.socket;

import irc4j.IRC4J;
import irc4j.io.InputThread;
import irc4j.io.OutputThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SocketHandler {

    private Socket socket;
    private InetSocketAddress address;
    private IRC4J irc4J;

    public SocketHandler() {
        this.socket = new Socket();
    }

    public SocketHandler(IRC4J irc4J, InetSocketAddress address) {
        this.socket = new Socket();
        this.irc4J = irc4J;
        this.address = address;
    }

    public void connect(String host, int port) throws IOException {
        socket.connect(new InetSocketAddress(host, port));
    }

    public void connect() throws IOException {
        socket.connect(address);
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public OutputThread createOutputThread() throws IOException {
        return new OutputThread(irc4J, new PrintWriter(socket.getOutputStream()));
    }

    public InputThread createInputThread() throws IOException {
        return new InputThread(irc4J, new BufferedReader(new InputStreamReader(socket.getInputStream())));
    }

}
