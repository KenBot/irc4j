package irc4j.io;

import irc4j.IRC4J;

import java.io.PrintWriter;
import java.util.concurrent.LinkedBlockingQueue;

public class OutputThread {

    private PrintWriter writer;
    private IRC4J irc4J;
    private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>();


    public OutputThread(IRC4J irc4J, PrintWriter writer) {
        this.writer = writer;
        this.irc4J = irc4J;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void sendRawLine(String line) {
        System.out.println("SENT: " + line);
        writer.println(line);
        writer.flush();
    }

    private void connectionCheck() {
        if (!irc4J.isConnected()) {
            throw new RuntimeException("IRC4J has not connected to the IRC server!");
        }
    }
}
