package irc4j.io;

import irc4j.IRC4J;

import java.io.BufferedReader;
import java.io.IOException;

public class InputThread implements Runnable {

    private BufferedReader reader;
    private IRC4J irc4J;
    private Thread runThread;
    private boolean stopThread;

    public InputThread(IRC4J irc4J, BufferedReader reader) {
        this.reader = reader;
        this.irc4J = irc4J;
    }

    public void start() {
        this.runThread = new Thread(this);
        runThread.start();
    }

    public void stop() {
        this.stopThread = true;
        if (runThread.isAlive()) {
            runThread.interrupt();
        }
    }

    @Override
    public void run() {
        String line;
        this.stopThread = false;
        try {
            while ((line = reader.readLine()) != null) {
                if (!irc4J.getSocket().isConnected()) {
                    this.stopThread = true;
                }
                if (stopThread) {
                    break;
                }
                Thread handleLineThread = new Thread(new HandleLineThread(line));
                handleLineThread.start();
                if (stopThread) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class HandleLineThread implements Runnable {

        private String line;

        public HandleLineThread(String line) {
            this.line = line;
        }

        @Override
        public void run() {
            irc4J.handleLine(line);
        }
    }

}
