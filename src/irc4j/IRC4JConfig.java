package irc4j;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class IRC4JConfig {

    private InetSocketAddress address;
    private String nickname;
    private String username;
    private String realname;

    public IRC4JConfig() {

    }

    public void setServer(String hostname, int port) {
        this.address = new InetSocketAddress(hostname, port);
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRealName(String realName) {
        this.realname = realName;
    }

    public InetSocketAddress getSocketAddress() {
        return address;
    }

    public IRC4J create() {
        return new IRC4J(this);
    }

    public String getNickname() {
        return nickname;
    }

    public String getRealname() {
        return realname;
    }

    public String getUsername() {
        return username;
    }
}
