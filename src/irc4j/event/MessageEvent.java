package irc4j.event;

import irc4j.Event;
import irc4j.IRC4J;

public class MessageEvent extends Event {
    private String user;
    private String message;
    private String channel;
    private IRC4J bot;

    public MessageEvent(IRC4J bot, String user, String channel, String message) {
        this.user = user;
        this.channel = channel;
        this.message = message;
        this.bot = bot;
    }

    public String getUser() {
        return user;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public IRC4J getBot() {
        return bot;
    }
}
