package irc4j.event;

import irc4j.Event;
import irc4j.IRC4J;

public class ConnectEvent extends Event {

    private IRC4J bot;

    public ConnectEvent(IRC4J bot) {
        this.bot = bot;
    }

    public IRC4J getBot() {
        return bot;
    }
}
