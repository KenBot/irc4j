package irc4j;

import irc4j.event.ConnectEvent;
import irc4j.event.MessageEvent;
import irc4j.io.InputThread;
import irc4j.io.OutputThread;
import irc4j.socket.SocketHandler;
import irc4j.utils.IRCUtils;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class IRC4J {

    private SocketHandler socketHandler;
    private InputThread inputThread;
    private OutputThread outputThread;
    private IRC4JConfig config;
    private EventListener listener;
    private boolean connectionReady = false;
    private boolean hasConnected = false;

    protected IRC4J(IRC4JConfig config) {
        this.config = config;
        this.socketHandler = new SocketHandler(this, config.getSocketAddress());
    }

    public void connect() throws IOException {
        socketHandler.connect();
        this.inputThread = socketHandler.createInputThread();
        this.outputThread = socketHandler.createOutputThread();
        inputThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        outputThread.sendRawLine("NICK " + config.getNickname());
        outputThread.sendRawLine("USER " + config.getUsername() + " 8 * : " + config.getRealname());
    }

    public void setEventListener(EventListener listener) {
        this.listener = listener;
    }

    public Socket getSocket() {
        return socketHandler.getSocket();
    }

    public void handleLine(String line) {
        List<String> raw = IRCUtils.tokenizeLine(line);

        if (raw.get(0).equals("PING")) {
            outputThread.sendRawLine("PONG :" + raw.get(1));
            if (!connectionReady) {
                this.connectionReady = true;
            }
        } else if (raw.get(1).equals("NOTICE")) {
            StringBuilder builder = new StringBuilder();
            for (int i = 3; i<raw.size(); i++) {
                builder.append(raw.get(i)).append(" ");
            }
            System.out.println("NOTICE FROM " + raw.get(0) + ": " + builder.toString());
        } else if (raw.get(1).equals("PRIVMSG")) {
            MessageEvent event = new MessageEvent(this, raw.get(0), raw.get(2), raw.get(3));
            listener.onMessageEvent(event);
        }
        if (connectionReady || !hasConnected) {
            listener.onConnectEvent(new ConnectEvent(this));
            this.connectionReady = false;
            this.hasConnected = true;
        }
    }

    public void disconnect() {
        sendRawLine("QUIT :IRC4J has disconnected.");
        inputThread.stop();
        this.hasConnected = false;
        this.connectionReady = false;
    }

    public boolean isConnected() {
        return socketHandler.isConnected();
    }

    public void sendRawLine(String line) {
        outputThread.sendRawLine(line);
    }
}
